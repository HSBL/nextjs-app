import Link from "next/link";

export default function Games() {
  return (
    <div>
      <h1>Halaman Games</h1>
      <Link href="/games/batu-kertas-gunting">
        <button className="btn btn-primary">batu kertas gunting</button>
      </Link>
    </div>
  );
}
