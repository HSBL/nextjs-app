import Link from "next/link";

export default function BatuKertasGunting() {
  return (
    <div className="container text-center">
      <h1>Batu Kertas Gunting</h1>
      <Link href="/">
        <button className="btn btn-primary">Back to home</button>
      </Link>
    </div>
  );
}
